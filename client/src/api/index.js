const axios = require('axios');

const fetchNotes = async (apiURL) =>
	await axios
		.get(apiURL + '/notes')
		.then((res) => res)
		.catch((err) => {
			throw err;
		});

const postNote = async (apiURL, postData) =>
	await axios
		.post(apiURL + '/notes', postData)
		.then((res) => res)
		.catch((err) => {
			throw err;
		});

module.exports = {
	fetchNotes,
	postNote,
};

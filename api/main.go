package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
)

func main() {

	app := fiber.New()
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))
	app = Endpoints(app)

	app.Listen(":5000")

}

func Endpoints(app *fiber.App) *fiber.App {
	l := List{
		Notes: []Model{},
	}

	app.Get("/notes", l.GetHandler)
	app.Post("/notes", PostHandler)

	return app
}

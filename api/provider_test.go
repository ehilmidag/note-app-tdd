package main

import (
	"testing"

	"github.com/gofiber/fiber/v2"
	"github.com/pact-foundation/pact-go/types"
	"gopkg.in/pact-foundation/pact-go.v1/dsl"
)

func Test_Provider(t *testing.T) {
	pact := &dsl.Pact{
		Provider: "note-app-backend",
	}

	pact.VerifyProvider(t, types.VerifyRequest{
		ProviderBaseURL:            "http://127.0.0.1:5000",
		BrokerURL:                  "https://ehilmidag.pactflow.io",
		BrokerToken:                "tXCE0GHKuVvglcFKTJChGw",
		PublishVerificationResults: true,
		ProviderVersion:            "22a-10e-104cc",

		BeforeEach: func() error {
			app := fiber.New()
			app = Endpoints(app)
			go app.Listen(":5000")

			return nil
		},
		StateHandlers: types.StateHandlers{
			"if i send  get request to /notes": func() error {

				// m1 := Model{ID: "1", Note: "lorem ipsum"}
				// m2 := Model{ID: "2", Note: "lorem hamza"}
				// m3 := Model{ID: "3", Note: "lorem abbas"}

				// AddNote(&m1)
				// AddNote(&m2)
				// AddNote(&m3)

				return nil
			},
			"if i send  post request to /notes": func() error {
				return nil
			},
		},
	})
}
